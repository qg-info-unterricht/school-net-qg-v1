import java.util.ArrayList;

/**
 * The NewsFeed class stores news posts for the news feed in a social network 
 * application.
 * 
 * Display of the posts is currently simulated by printing the details to the
 * terminal. (Later, this should display in a browser.)
 * 
 * This version does not save the data to disk, and it does not provide any
 * search or ordering functions.
 * 
 * @author Michael Kölling and David J. Barnes
 * @version 0.1
 */
public class NewsFeed
{
    private ArrayList<TextBeitrag> messages;
    private ArrayList<PhotoBeitrag> photos;

    /**
     * Construct an empty news feed.
     */
    public NewsFeed()
    {
        messages = new ArrayList<>();
        photos = new ArrayList<>();
    }

    /**
     * Fuege einen PhotoBeitrag in den Newsfeed ein.
     * 
     * @param text  The text post to be added.
     */
    public void addTextBeitrag(TextBeitrag text)
    {
        messages.add(text);
    }

    /**
     * Fuege einen PhotoBeitrag in den Newsfeed ein
     * 
     * @param photo  Das einzufuegende PhotoBeitrag-Objekt
     */
    public void addPhotoBeitrag(PhotoBeitrag photo)
    {
        photos.add(photo);
    }

    /**
     * Zeige die Beitraege in einer Liste an
     * Zuerst mal nur auf der Textkonsole.
     */
    public void showFeed()
    {
        // display all text posts
        for(TextBeitrag message : messages) {
            message.display();
            System.out.println();   // empty line between posts
        }

        // display all photos
        for(PhotoBeitrag photo : photos) {
            photo.display();
            System.out.println();   // empty line between posts
        }
    }
}
